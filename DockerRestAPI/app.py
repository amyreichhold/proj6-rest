"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
#from flask import request, redirect
from flask import Flask, redirect, url_for, request, render_template, make_response
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import json

import logging
from flask_cors import CORS

###
# Globals
###
app = flask.Flask(__name__)
cors = CORS(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient('db', 27017)
#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#db = client.tododb
db = client.badguy.db

###
# Pages
###

@app.route("/listall/<representation>")
@app.route("/listAll/<representation>")
def listAll(representation):
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = []
    for controle in _controles:
        hamstring = {}
        for key, value in controle.items():
            if key != '_id':
                hamstring[key] = value
        if hamstring['km'] != "":
            controles.append(hamstring)
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    truncate = len(controles)
    top = request.args.get('top')
    if top is not None:
        truncate = int(top)
        if truncate > len(controles):
            truncate = len(controles)
        elif truncate < 0:
            truncate = 0
        else:
            pass
    controles = controles[:truncate]

    if representation.lower() == 'json':
        potato = {
                 'controles': controles
            }
        return flask.jsonify(potato)
    elif representation.lower() == 'csv':
        headers = []
        big_boi = ''
        first_controle = controles[0]
        for key in first_controle:
            headers.append(key)
        big_boi += ",".join(headers) + '\n'
        for controle in controles:
            row = []
            for header in headers:
                row.append(controle[header])
            big_boi += ",".join(row) + '\n'
        response = make_response(big_boi, 200)
        response.mimetype = "text/plain"
        return response
    else:
        return 'not today satan'

@app.route("/listall")
@app.route("/listAll")
def listAllJson():
    return listAll('json')

@app.route("/listopenonly/<representation>")
@app.route("/listOpenOnly/<representation>")
def listOpenOnly(representation):
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = []
    for controle in _controles:
        hamstring = {}
        for key, value in controle.items():
            if key != '_id' and key != 'close':
                hamstring[key] = value
        if hamstring['km'] != "":
            controles.append(hamstring)
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    truncate = len(controles)
    top = request.args.get('top')
    if top is not None:
        truncate = int(top)
        if truncate > len(controles):
            truncate = len(controles)
        elif truncate < 0:
            truncate = 0
        else:
            pass
    controles = controles[:truncate]

    if representation.lower() == 'json':
        potato = {
                 'controles': controles
            }
        return flask.jsonify(potato)
    elif representation.lower() == 'csv':
        headers = []
        big_boi = ''
        first_controle = controles[0]
        for key in first_controle:
            headers.append(key)
        big_boi += ",".join(headers) + '\n'
        for controle in controles:
            row = []
            for header in headers:
                row.append(controle[header])
            big_boi += ",".join(row) + '\n'
        response = make_response(big_boi, 200)
        response.mimetype = "text/plain"
        return response
    else:
        return 'not today satan'

@app.route("/listOpenOnly")
@app.route("/listopenonly")
def listOpenOnlyJson():
    return listOpenOnly('json')

@app.route("/listcloseonly/<representation>")
@app.route("/listCloseOnly/<representation>")
def listCloseOnly(representation):
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = []
    for controle in _controles:
        hamstring = {}
        for key, value in controle.items():
            if key != '_id' and key != 'open':
                hamstring[key] = value
        if hamstring['km'] != "":
            controles.append(hamstring)
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    truncate = len(controles)
    top = request.args.get('top')
    if top is not None:
        truncate = int(top)
        if truncate > len(controles):
            truncate = len(controles)
        elif truncate < 0:
            truncate = 0
        else:
            pass
    controles = controles[:truncate]

    if representation.lower() == 'json':
        potato = {
                 'controles': controles
            }
        return flask.jsonify(potato)
    elif representation.lower() == 'csv':
        headers = []
        big_boi = ''
        first_controle = controles[0]
        for key in first_controle:
            headers.append(key)
        big_boi += ",".join(headers) + '\n'
        for controle in controles:
            row = []
            for header in headers:
                row.append(controle[header])
            big_boi += ",".join(row) + '\n'
        response = make_response(big_boi, 200)
        response.mimetype = "text/plain"
        return response
    else:
        return 'not today satan'

@app.route("/listCloseOnly")
@app.route("/listcloseonly")
def listCloseOnlyJson():
    return listCloseOnly('json')


@app.route("/")
@app.route("/index", methods=['GET', 'POST'])
def index():
    app.logger.debug("Main page entry")
    if request.method == 'POST':
        db.brevet.drop()
        db.controle.drop()
        item_doc = {
            'distance': request.form['distance'],
            'begin_date': request.form['begin_date'],
            'begin_time': request.form['begin_time']
        }
        db.brevet.insert_one(item_doc)
        item_docs = []
        for i,miles in enumerate(request.form.getlist('miles')):
            item_docs.append({
                    'miles': miles
                    })
        for i,km in enumerate(request.form.getlist('km')):
            item_docs[i]['km'] = km
        for i,location in enumerate(request.form.getlist('location')):
            item_docs[i]['location'] = location
        for i,open in enumerate(request.form.getlist('open')):
            item_docs[i]['open'] = open
        for i,close in enumerate(request.form.getlist('close')):
            item_docs[i]['close'] = close

        for item_doc in item_docs:
            db.controle.insert_one(item_doc)

    return flask.render_template('calc.html')

@app.route('/new', methods=['POST'])
def potato():
    item_doc = {
        'distance': request.form['distance'],
        'begin_date': request.form['begin_date']
    }
    
    db.tododb.insert_one(item_doc)
    
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html')
    #return str('{distance} {begin_date}').format(distance=item_doc["distance"], begin_date=item_doc["begin_date"])


@app.route('/display')
def display():
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = [controle for controle in _controles]
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    return render_template('display.html', items=items, controles=controles, message=message)
    #return redirect(url_for('index')) 


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_start_time = request.args.get('brevet_start_time', "2017-01-01T00:00:00-07:00", type=str)
    brevet_dist_km = request.args.get('brevet_dist_km', 200, type=int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, brevet_start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
