<html><head>
<title>ACP Controle Times Display</title>
<script
  src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
</script>
</head>
<body>

Billie

<form>

<label>Top</label>
<select id="top">
    <option value="1">1</option>
    <option value="2" selected="selected">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
</select>

<label>List controle times:</label>
<input type="submit" value="All" onclick="return listAll()"></input>
<input type="submit" value="Open Only" onclick="return listOpenOnly()"></input>
<input type="submit" value="Close Only" onclick="return listCloseOnly()"></input>
</form>

<table id="bad_boi"></table>

<script type="text/javascript">

function listControles(chosen) {
    console.log("chosen: " + chosen);
    //var shots = $("#top").val();
    var shots = document.getElementById("top").value;
    console.log("shots: " + shots);
    $.getJSON('http://localhost:5001/list' + chosen + '?top=' + shots,
        // response handler
        function(data) {
            if (data.controles.length > 0) {
                // https://api.jquery.com/jQuery.getJSON/
                var items = [];

                // get the values for each controle
                var heads_up = 0;
                $.each(data.controles, function(index, controle) {
                    // if we haven't yet, get the headers
                    if (heads_up == 0) {
                        items.push("<tr>");
                        $.each(controle, function(key, value) {
                            items.push("<td>" + JSON.stringify(key) + "</td>");
                        });
                        items.push("</tr>");
                        heads_up = 1;
                    };

                    items.push("<tr>");
                    $.each(controle, function(key, value) {
                        items.push("<td>" + JSON.stringify(value) + "</td>");
                    });
                    items.push("</tr>");
                });
  
                $("#bad_boi").empty().append(items.join(""));
            };
      } // end of handler function
    ); // End of getJSON
    return false;
};

function listAll() {
    return listControles("All");
};

function listOpenOnly() {
    return listControles("OpenOnly");
};

function listCloseOnly() {
    return listControles("CloseOnly");
};

</script>

</body>
</html>
